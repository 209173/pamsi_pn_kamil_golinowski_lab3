#include <iostream>
#include <stack>
using namespace std;


class Stos{
	
	
	public:
		stack<int> Stos;
		
		void Dodaj(int wpis){
			Stos.push(wpis);			
		}
		
		int Usun(){
			if(Stos.empty()){
				cout<<"Blad! Pusta!"<<endl;
			}
			else{
			int tmp=Stos.top();
			Stos.pop();
			return tmp;
			}
			
		}
		
		void Wyswietl(int wysokosc){
				for(int i=0; i<wysokosc; i++){
					cout<<Stos.top()<<" ";
					Stos.pop();
				}
		}
		
		int Rozmiar(){
			return Stos.size();
		}
		
	bool pusty(){
		return Rozmiar();
	}

};

void PrzeniesKlocek(Stos &wszystkie, Stos &wynik){						//funkcja przenoszaca klocek z jednego stosu na drugi
	wynik.Dodaj(wszystkie.Usun());
}

void Hanoi(Stos &wszystkie, Stos &wynik, int n) {						//funkcja realicujaca algorytm wiezy Hanoi
Stos pomoc;																//pomocniczy stos, "srodkowy" pal 

	if(n>1){															//jesli ilosc klockow na 1szym palu jest wieksza niz jeden
		Hanoi(wszystkie,pomoc,n-1);										//przenosimy n-1 klockow na pal pomocniczy
		Hanoi(wszystkie,wynik,1);										//nastepnie ostatni klocek z 1ego pala przenosimy na pal glowny
		Hanoi(pomoc,wynik,n-1);											//ostatecznie przenosimy n-1 klockow z srodkowego pala na pal glowny
	}
	else{
		PrzeniesKlocek(wszystkie,wynik);								//jesli na palu 1szym jest 1 klocek, przenosimy go na pal glowny
	}
}

int main(){
	Stos wszystkie, wynik;
	int wysokosc, tmp;
	
	cout<<"Podaj jaka wysokosc ma miec wieza: ";
	cin>>wysokosc;										//wczytanie wysokosci wiezy
	cout<<"Podaj zawartosc wiezy: "<<endl;				//wczytanie zawartosci wiezy	
		
		for(int i=0; i<wysokosc; i++){							
			cin>>tmp;
			wszystkie.Dodaj(tmp);
		}
		cout<<endl;
		
	Hanoi(wszystkie,wynik, wysokosc);					//wykonanie przemieszczenia z 1szego pala na pal glowny
	cout<<"Rezultat"<<endl;
		wynik.Wyswietl(wysokosc);						//wyswietlenie rezultatu czyli pala glownego
	
	
	return 0;
}



#include <iostream>
#include <list>
#include <stdlib.h>
#define ROZMIAR 100
using namespace std;


//klasa realizujaca pojecie kolejki jako okrag o zadanej wczesniej dlugosci ROZMIAR
class Kolejka_tab{
	public:
		Kolejka_tab(){							//konstruktor bezparametryczny
			pierwszy=ostatni=-1;			
			zawartosc=new int[ROZMIAR];
			rozmiar=ROZMIAR;
		}
		void Zwieksz_Rozmiar(){
			int tmp=this->rozmiar;
			tmp*=2;
			this->rozmiar=tmp;
		}
		
		bool pelna() {				//kolejka jest pelna jesli element pierwszy jest na 1 a element ostatni jest na ROZMIAR miejscu tablicy 
											//lub ostatni jest tuz za pierwszym
			if(pierwszy==0 && ostatni==rozmiar-1){
				int i=2;
				realloc(zawartosc, 2*rozmiar*sizeof(int));
				Zwieksz_Rozmiar();
				return true;
				}
			else 
				return false;		
		}
		
		bool pusta() const {
			return pierwszy==-1 && ostatni==-1;
		}
		
		void Dodaj(int wpis){							//funkcja dodajaca element do kolejki
			if(!pelna()){								//mozna dodac element tylko jesli kolejka jest niepelna
				if(ostatni==ROZMIAR-1||ostatni==-1)	{	//jesli ostatni element jest juz na ROZMIAR miejscu, lub kolejka jest pusta
					zawartosc[0]=wpis;					//zapis nastepuje na pierwszym miejscu w kolejce
					ostatni=0;							//wtedy dany element jest zarowno ostatni
					if(pierwszy==-1)
						pierwszy=0;						//jak i pierwszy
				}
				else{
					ostatni++;
					zawartosc[ostatni]=wpis;			//w przeciwnym wypadku element dodany jest jako nastepny w kolejce
				}
			}	
			
			else 										//jesli kolejka jest pelna, wyswietli sie odpowiedni komunikat
				cout<<"Kolejka jest pelna!"<<endl;
		}
		
		int Usun(){										//funkcja usuwajaca pierwszy element z kolejki
			int i=zawartosc[pierwszy];					//zmienna tymczasowa, przypisanie do pierwszego elementu kolejki
			if(pierwszy==ostatni)						//jesli pierwszy i ostatni element to to samo
				pierwszy=ostatni=-1;					//wtedy kolejka konczy sie i zmienne wracaja do wartosci poczatkowych
			else 									
				if(pierwszy==ROZMIAR-1)					//jesli pierwszy element jest na ROZMIAR miejscu, licznik sie "przekreca"
					pierwszy=0;							//i pierwszym elementem jest element na miejscu 1 w kolejce
				else
					pierwszy++;							//jesli nastapila inna sytuacja, nastepuje przesuniecie kolejki
			return i;									//funkcja zwraca wartosc usunietego elementu
		}
		
		void Wyswietl() const {								
			if(pierwszy<=ostatni)							//jesli element pierwszy jest blizej poczatku tablicy niz element ostatni
				for(int i=pierwszy; i<ostatni+1; i++){		//wtedy wykonuje sie zwykla petla od pierwszego do ostatniego
					cout<<zawartosc[i]<<" ";				//wypisz zawartosc
				}
			else{
				for(int i=pierwszy; i<ROZMIAR; i++){		//jesli natomiast pierwszy jest dalej niz ostatni
					cout<<zawartosc[i]<<" ";				//wyswietlana jest zawartosc od pierwszego do zawartosc[ROZMIAR-1]
				}
				for(int i=0; i<ostatni; i++){				//i od zawartosc[0] do ostatniego
					cout<<zawartosc[i]<<" ";
				}
			}
		}
		
		void menu_tab(){
			int opcja, nowa;
			char *s=(char *)"PUSTA LISTA!";	
			do{
				cout << "1.Dodaj wartosc do kolejki"<<endl;
				cout << "2.Usun wartosc z kolejki"<<endl;
				cout << "3.Wyswietl kolejke"<<endl;
				cout << "0.Koniec"<<endl;
				cout << "Wybierz opcje: ";
				cin >> opcja;
				cout<<endl;
				switch(opcja)
			{
 				case 0: break; 
 				case 1: {
 							cout<<"Podaj wartosc ktora chcesz dodac: ";
 							cin>>nowa;
 		 					this->Dodaj(nowa);
 		 					break;
 	   	 		}	
 				case 2:{
 							if(!this->pusta())
 								cout<<"Zostala usunieta liczba: "<<this->Usun()<<endl;
							else
								throw(s);
							break;
		 		}		 
 				case 3:{
 							if(!this->pusta())
 								this->Wyswietl();
							else
								throw(s);
							break;
 				}
			}

				cout<<endl<<endl;
		} while(opcja!=0);
}
			
	private:
		int pierwszy, ostatni, rozmiar;
		int *zawartosc;
};

//pojecie definiujace kolejke w oparciu o standardowe biblioteki
class Kolejka_stl {
	list<int> lista;
	
	public:
	void Dodaj(int elem){
		lista.push_back(elem);
	}
	
	int Usun(){
		int dana=lista.front();
		lista.pop_front();
		return dana;	
	}
	
	void Usun_cala(){
		char *s=(char *)"PUSTA LISTA!";
		if(this->Rozmiar()){
		lista.clear();
		}
		else
			throw(s);
	}
	int Rozmiar(){
		return	lista.size();
	}
	
	void Wyswietl(){
		list<int>::iterator it;
   		for( it=lista.begin(); it!=lista.end(); ++it )
   		{
      	   cout<<*it<<" ";  
   		}
	}
	
	void menu_stl(){
		int opcja, nowa;
		char *s=(char *)"PUSTA LISTA!";	
			do{
				cout << "1.Dodaj wartosc do kolejki"<<endl;
				cout << "2.Usun wartosc z kolejki"<<endl;
				cout << "3.Usun cala kolejke"<<endl;
				cout << "4.Wyswietl kolejke"<<endl;
				cout << "0.Koniec"<<endl;
				cout << "Wybierz opcje: ";
				cin >> opcja;
				cout<<endl;
				switch(opcja)
			{
 				case 0: break; 
 				case 1: {
 							cout<<"Podaj wartosc ktora chcesz dodac: ";
 							cin>>nowa;
 		 					this->Dodaj(nowa);
 		 					break;
 	   	 		}	
 				case 2:{
 							if(this->Rozmiar())
 								cout<<"Zostala usunieta liczba: "<<this->Usun()<<endl;
							else
								throw (s);
							break;
		 		}	
				case 3:{
					this->Usun_cala();
					cout<<"Usunieto!";
					break;
				} 
 				case 4:{
 							if(this->Rozmiar())
 								this->Wyswietl();
							else
								throw (s);
							break;
 				}
			}

				cout<<endl<<endl;
		} while(opcja!=0);
	}
};



int main(){
		Kolejka_tab przyklad;
		Kolejka_stl kolej;
		int opcja;
		
		cout<<"Jesli chcesz skorzystac z kolejki tablicowej, wcisnij 1."<<endl;
		cout<<"Jesli chcesz skorzystac z kolejki STL, wcisnij 0."<<endl;
		cin>>opcja;
		
		if(opcja==1){
			try{
			przyklad.menu_tab();
			}
			catch(char *s){
				cout<<"Blad: "<<s<<endl;	
			}	
		}
		else{
			try{
			kolej.menu_stl();
			}
			catch(char *s){
				cout<<"Blad: "<<s<<endl;	
			}
			
		}	
	return 0;
}



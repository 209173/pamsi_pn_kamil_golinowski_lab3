#include <iostream>
using namespace std;

//funkcja liczaca mediane
template <typename T>
T mediana(T a, T b, T c){
	T med=max(min(a, b), min(max(a, b), c)); //zwraca element srodkowy zbioru 3 liczb
}
//funkcja wybierajaca pivota i dzielaca tablice na dwie podtablice- elementy mniejsze i wieksze od pivota
template <typename T>
T partycjonowanie(T dane[], int poczatek, int koniec){
	T pivot=mediana(dane[poczatek],dane[koniec],dane[(poczatek+koniec)/2]);		//pivot wybierany jako mediana elementow pierwszego,
	int i=poczatek;																	//srodkowego i ostatniego
    int j=koniec;
    T temp;																		//dana tymczasowa do zamiany dwoch elementow na tablicy
    do
    {
		 while (pivot<dane[j])    j--;							//najpierw wyszukujemy pierwszy element z prawej podtablicy ktory jest mniejszy od pivota
         while (pivot>dane[i])    i++;							//nastepnie wyszukujemy pierwszy element z lewej podtablicy ktory jest wiekszy od pivota
          if (i < j)											//jesli nie przekroczylismy zakresu
         { 
                 temp=dane[i];    								//zamieniamy obie te wartosci
                 dane[i]=dane[j];								//uzywajac zmiennej pomocniczej
                 dane[j]=temp;
                 i++;
                 j--;
         }
     }while (i < j);    										//petla wykonuje sie tak dlugo, dopoki nie skoncza sie obie podtablice
	  
     return j;     												//funkcja zwraca polozenie pivota
	
}

template <typename T>
void sortowanie_szybkie(T dane[], const int poczatek, const int koniec){
	if(poczatek<koniec){
	    T pivot=partycjonowanie(dane, poczatek, koniec); 
		sortowanie_szybkie(dane,poczatek,pivot);
    	sortowanie_szybkie(dane,pivot+1,koniec); 	
	}
}

int main(){
	int *dane;
	int n;
	
	cout<<"Podaj rozmiar: ";
	cin>>n;
	dane = new int[n];
	cout<<"Podaj dane: "<<endl;
	for(int i=0; i<n;i++)
		cin>>dane[i];
	sortowanie_szybkie(dane, 0, n-1);
	
	cout<<"Wynik: ";
	for(int j=0;j<n;j++)
		cout<<dane[j]<<" ";
}

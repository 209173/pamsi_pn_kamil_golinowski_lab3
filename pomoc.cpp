#include <iostream>
#include <stdlib.h>
#include <Windows.h>
using namespace std;


LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}
LARGE_INTEGER endTimer()
{
LARGE_INTEGER stop;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&stop);
SetThreadAffinityMask(GetCurrentThread(), oldmask);
return stop;
}
int main(){
LARGE_INTEGER performanceCountStart,performanceCountEnd;
performanceCountStart = startTimer(); //zapamiętujemy czas początkowy
for (int i=0;i<10; i++){ //tutaj funkcje, których mierzymy wydajność
	cout<<i<<" ";
	}
performanceCountEnd = endTimer(); //zapamiętujemy koniec czasu
double tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
cout << endl << "Time:" <<tm <<endl;
}


	

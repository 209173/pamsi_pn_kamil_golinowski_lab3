#include <iostream>
using namespace std;

int *pom; //tablica pomocnicza

template <typename T>
void scal(T dane[], int poczatek, int koniec){
	int srodek=(poczatek+koniec)/2;			//ustalenie srodka
	int i1=poczatek;						//ustawienie indeksu dla tablicy pomocniczej
	int i2=poczatek;						//oraz dla tablic do scalenia
	int i3=srodek+1;						//od poczatku do srodka oraz od srodka do konca
	
	for(int i=poczatek;i<=koniec; i++){		//przepisanie danych do tablicy pomocniczej
		pom[i]=dane[i];
	}
	
	while(i2<=srodek && i3<=koniec){		//petla porownujaca dwa elementy dwoch podtablic, dopoki ktoras z nich sie nie skonczy  
		if(pom[i2]<pom[i3]){				//jesli element z pierwszej jest mniejszy niz z drugiej
			dane[i1++]=pom[i2++];			//do wyjsciowej tablicy zapisany jest element z pierwszej
		}
		else
			dane[i1++]=pom[i3++];			//w przeciwnym wypadku do wyjsciowej zapisany jest element z drugiej
	}
	while(i2<=srodek)						//dopisanie wartosci w przypadku gdyby szybciej skonczyla sie druga tablica
		dane[i1++]=pom[i2++];
}

template <typename T>
void sortscalanie(T dane[], int poczatek, int koniec){
	if(koniec<=poczatek) 					//sytuacja gdy mamy jeden element
		return;
		
	int srodek=(poczatek+koniec)/2;			//ustalenie srodka
	
	sortscalanie(dane, poczatek, srodek);	//dzielimy na dwie tablice, od poczatku do srodka
	sortscalanie(dane, srodek+1, koniec);	//i od srodka do konca
	scal(dane, poczatek, koniec);			//nastepnie scalamy dwie tablice
	
}


int main(){
	int *dane;
	int n;
	
	cin>>n;
	dane = new int[n];
	pom = new int [n];
	cout<<"Podaj dane: "<<endl;
	for(int i=0; i<n;i++)
		cin>>dane[i];
	
	sortscalanie(dane, 0, n-1);
	
	cout<<"Wynik: ";
	for(int j=0;j<n;j++)
		cout<<dane[j]<<" ";
	
}

#include <iostream>
#include <stack>
#include <stdlib.h>
#include <Windows.h>
#define ROZMIAR 500000
using namespace std;
////////////////////////////////////////////////////////////////////////////////////////////TABLICA
class Stos_tab{
	public:
		Stos_tab(){							//konstruktor bezparametryczny
			pierwszy=ostatni=-1;			
			zawartosc=new int[ROZMIAR];
			rozmiar=ROZMIAR;
		}
		
		bool pusty() const {
			return pierwszy==-1 && ostatni==-1;
		}
		
		void Dodaj(int wpis){							//funkcja dodajaca element do stosu							
				if(ostatni==-1)	{						//jesli stos jest pusty
					zawartosc[0]=wpis;					//zapis nastepuje na pierwszym miejscu w stosie
					ostatni=0;							//wtedy dany element jest zarowno ostatni
					if(pierwszy==-1)
						pierwszy=0;	
									}					//jak i pierwszy
				else{
					ostatni++;
					zawartosc[ostatni]=wpis;			//w przeciwnym wypadku element dodany jest jako nastepny w kolejce
				}
			}
		
		int Usun(){										//funkcja usuwajaca ostatni element ze stosu
		char *s=(char *)"PUSTA LISTA!";	
			if(this->pusty())
				throw(s);
			else{
			int i=zawartosc[ostatni];					//zmienna tymczasowa, przypisanie do niej ostatniego elementu stosu
			if(ostatni==pierwszy)						//jesli pierwszy i ostatni element to to samo
				pierwszy=ostatni=-1;					//wtedy stos zeruje sie-jest pusty
			else 									
				ostatni--;								//jesli nastapila inna sytuacja, nastepuje usuniecie elementu stosu
			return i;									//funkcja zwraca wartosc usunietego elementu
			}
		}
		
		void Wyswietl() const {			
		char *s=(char *)"PUSTA LISTA!";	
		if(this->pusty())
			throw(s);	
		else{									
				for(int i=pierwszy; i<ostatni+1; i++){		
					cout<<zawartosc[i]<<" ";				
				}
			}
		}
			
		
		
		void menu_tab(){
			int opcja, nowa;
			do{
				cout << "1.Dodaj wartosc do stosu"<<endl;
				cout << "2.Usun wartosc ze stosu"<<endl;
				cout << "3.Wyswietl stos i usun go"<<endl;
				cout << "0.Koniec"<<endl;
				cout << "Wybierz opcje: ";
				cin >> opcja;
				cout<<endl;
				switch(opcja)
			{
 				case 0: break; 
 				case 1: {
 							pelny();
 							cout<<"Podaj wartosc ktora chcesz dodac: ";
 							cin>>nowa;
 		 					this->Dodaj(nowa);
 		 					break;
 	   	 		}	
 				case 2:{	try{
							cout<<"Zostala usunieta liczba: "<<this->Usun()<<endl;
 							}
							catch(char *s){
							
								cerr<<"Blad: "<< s <<endl;
							}
							break;
		 		}		 
 				case 3:{
 							try{
							this->Wyswietl();
 							}
							catch(char *s){
							
								cerr<<"Blad: "<< s <<endl;
							}
 								
							break;
 				}
			}

				cout<<endl<<endl;
		} while(opcja!=0);
		
		
}
		int Rozmiar(){
			return this->rozmiar;
		}	
	private:
		int pierwszy, ostatni, rozmiar;
		int *zawartosc;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////STL
class Stos_stl{
	stack<int> Stos;
	
	public:
		void Dodaj(int wpis){
			Stos.push(wpis);			
		}
		
		void Usun(){
			char *s=(char *)"PUSTA LISTA!";
			if(Stos.empty()){
				throw(s);
			}
			else
			Stos.pop();
			
		}
		
		void Wyswietl(){
			char *s=(char *)"PUSTA LISTA!";
			if(Stos.empty()){
				throw(s);
			}
			else
				for(int i=0; i<Stos.size(); i++){
					cout<<Stos.top()<<" ";
					Stos.pop();
				}
		}
			
		void menu_stl(){
			int opcja, nowa;
			do{
				cout << "1.Dodaj wartosc do stosu"<<endl;
				cout << "2.Usun wartosc ze stosu"<<endl;
				cout << "3.Wyswietl stos"<<endl;
				cout << "0.Koniec"<<endl;
				cout << "Wybierz opcje: ";
				cin >> opcja;
				cout<<endl;
				switch(opcja)
			{
 				case 0: break; 
 				case 1: {
 							cout<<"Podaj wartosc ktora chcesz dodac: ";
 							cin>>nowa;
 		 					this->Dodaj(nowa);
 		 					break;
 	   	 		}	
 				case 2:{	try{
							cout<<"Zostala usunieta liczba: "<<Stos.top()<<endl;
							this->Usun();
 							}
							catch(char *s){
							
								cerr<<"Blad: "<< s <<endl;
							}
							break;
		 		}		 
 				case 3:{
 							try{
							this->Wyswietl();
 							}
							catch(char *s){
							
								cerr<<"Blad: "<< s <<endl;
							}
 								
							break;
 				}
			}

				cout<<endl<<endl;
		} while(opcja!=0);
}
};

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}
LARGE_INTEGER endTimer()
{
LARGE_INTEGER stop;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&stop);
SetThreadAffinityMask(GetCurrentThread(), oldmask);
return stop;
}



int main(){
	LARGE_INTEGER performanceCountStart,performanceCountEnd;
	Stos_tab stos;
	Stos_stl Stos;



	performanceCountStart = startTimer(); //zapamiętujemy czas początkowy
for(int i=0;i<500000;i++){
			stos.Dodaj(rand());
			}
performanceCountEnd = endTimer(); //zapamiętujemy koniec czasu
long double tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;

cout << endl << "Czas tablicy: " <<tm <<endl;

	performanceCountStart = startTimer(); //zapamiętujemy czas początkowy
for(int i=0;i<500000;i++){
			Stos.Dodaj(rand());
			}
performanceCountEnd = endTimer(); //zapamiętujemy koniec czasu
tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
cout << endl << "Czas stl: " <<tm <<endl;

	return 0;
	
}
